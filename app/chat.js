'use strict';

window.onload = function() {

    var socket = io.connect('http://localhost:8000');

    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var userslist = document.getElementById("userslist");
    var name = document.getElementById("name");

    var users = [];
    var messages = [];
    var me = name.value;

    socket.emit('getList');
    inactivityTime();

    // process socket events
    socket
        .on('message', function (data) {
            if(data.message) {
                messages.push(data);
                var html = '';
					
					
                for(var i=0; i<messages.length; i++) {
                    html += '<b>' + '<span id=time>'+ (messages[i].time ? messages[i].time : "     " ) +'</span>' + (messages[i].username ? messages[i].username : 'Server') + ': </b>';
                    html += messages[i].message + '<br />';
                }
                content.innerHTML = html;
                content.scrollTop = content.scrollHeight;
            } else {
                console.log("There is a problem:", data);
            }
        })
        .on('leave', function(username) {
            var status = username + ' left  chat';
			console.log('status', status);
            socket.emit('getList');
        })
        .on('join', function(username) {
            var status = username + ' joined to chat';
            socket.emit('getList');
            console.log('status', status);
        })
        .on('connect', function() {
            console.log('connection established');
            socket.emit('getList');
        })
        .on('disconnect', function() {
            var status = 'connection lost';
            console.log('status', status);
        })
        .on('logout', function() {
            location.href = '/';
        })
        .on('list', function (_users) {
            users = _users;
            renderUsers(users);
        });

    // remove user from users list aster
    function inactivityTime (timeToInactive) {
        var t;
        timeToInactive = timeToInactive || 25000; // 25 sec

        resetTimer();

        document.onmousemove = resetTimer;
        document.onkeypress = resetTimer;


        function resetTimer() {
            socket.emit('getList');
            clearTimeout(t);
            t = setTimeout(disconnect, timeToInactive);// 25 sec
        }

        function disconnect() {
           users = users.filter(function (u) {
               return u !== me;
           });

            renderUsers(users);
        }
    }

    // render current users
    function renderUsers(users) {
        userslist.innerHTML = '';
        users.forEach(function (user) {
            if (user === me) {
                user = user + ' (me)';
            }
            // Create a <li> node
            var node = document.createElement("LI");
            node.setAttribute('class', 'online-user');
            var textnode = document.createTextNode(user);         // Create a text node
            node.appendChild(textnode);                              // Append the text to <li>
            userslist.appendChild(node);
        });
    }

	
	
    // process message sending
    var sendMessage = function sendMessage() {
        var text = field.value;
        if (!text) return;

        var MATH_REGEXP = /^([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)$/im;
        var MASS_REGEXP = /^(\d+)\s(kg|g|mg)\sto\s(kg|g|mg)$/im;
        var LENGTH_REGEXP = /^(\d+)\s(km|m|dm|cm|mm)\sto\s(km|m|dm|cm|mm)$/im;
        var USD_TO_EUR_REGEXP = /^(\d+)\s(usd|USD)\sto\s(eur|EUR)$/im;
        var EUR_TO_USD_REGEXP = /^(\d+)\s(eur|EUR)\sto\s(usd|USD)$/im;

        if (MATH_REGEXP.test(text)) {
            return emit('calculate', text);
        } else if (MASS_REGEXP.test(text)) {
            return emit('convertMass', text);
        } else if (LENGTH_REGEXP.test(text)) {
            return emit('convertLength', text);
        } else if (USD_TO_EUR_REGEXP.test(text)) {
            return emit('convertUSDtoEUR', text);
        } else if (EUR_TO_USD_REGEXP.test(text)) {
            return emit('convertEURoUSD', text);
        } else {
            return emit('send', text);
        }

		//check time
		function checkTime(i) {
			return (i < 10) ? "0" + i : i;
		}
	
        function emit(event, text) {
			var today = new Date(),
					h = checkTime(today.getHours()),
					m = checkTime(today.getMinutes()),
					s = checkTime(today.getSeconds());	
			var timenow = h + ':' + m + ':' + s;		
            socket.emit(event, { message: text, username: name.value , time: timenow});

            clearField();
        }

        function clearField () {
            field.value = "";
        }
    };

    sendButton.onclick = sendMessage;

    document.onkeydown = function(evt) {
        if (evt.keyCode === 13 && ['name', 'field'].indexOf(document.activeElement.id) > -1 ) {
            sendMessage();
        }
    };
};