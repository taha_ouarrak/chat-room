'use strict';

// Load the module dependencies
var config = require('../config/defaut'),
    fs = require('fs'),
    http = require('http'),
    cookieParser = require('cookie-parser'),
    passport = require('passport'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session),
    math = require('mathjs');

// Define the Socket.io configuration method
module.exports = function (server, db) {
    // Create a new Socket.io server
    var io = require('socket.io').listen(server);

    // Create a MongoDB storage object
    var mongoStore = new MongoStore({
        mongooseConnection: db.connection,
        collection: config.sessionCollection
    });

    // Intercept Socket.io's handshake request
    io.use(function (socket, next) {
        // Use the 'cookie-parser' module to parse the request cookies
        cookieParser(config.sessionSecret)(socket.request, {}, function (err) {
            // Get the session id from the request cookies
            var sessionId = socket.request.signedCookies ? socket.request.signedCookies[config.sessionKey] : undefined;

            if (!sessionId) return next(new Error('sessionId was not found in socket.request'), false);

            // Use the mongoStorage instance to get the Express session information
            mongoStore.get(sessionId, function (err, session) {
                if (err) return next(err, false);
                if (!session) return next(new Error('session was not found for ' + sessionId), false);

                // Set the Socket.io session information
                socket.request.session = session;
                // Use Passport to populate the user details
                passport.initialize()(socket.request, {}, function () {
                    passport.session()(socket.request, {}, function () {
                        if (socket.request.user) {
                            next(null, true);
                        } else {
                            next(new Error('User is not authenticated'), false);
                        }
                    });
                });
            });
        });
    });

    io.sockets.on('connection', function (socket) {
        //var username = (socket.request.user.local.username) ? socket.request.user.local.username : socket.request.user.displayName;
        var username = (socket.request.user.displayName) ? socket.request.user.displayName : socket.request.user.local.username;

        socket.emit('list',  getUsersList());
        socket.broadcast.emit('join', username);
        socket.emit('message', { message: 'welcome to the chat' });

        socket
            .on('getList', function() {
                socket.emit('list',  getUsersList());
            })
            .on('calculate', function (data) {
                if (!data.message) return;
    
                var MATH_REGEXP = /^([-+]?[0-9]*\.?[0-9]+[\/\+\-\*])+([-+]?[0-9]*\.?[0-9]+)$/im; // 5+9 5-9 5*5 9/3
    
                return processRegExpData(MATH_REGEXP, data);
            })
            .on('convertMass', function (data) {
                if (!data.message) return;
    
                var units = 'kg|g|mg';
                var MASS_REGEXP = defineRegExpByUnits(units);
    
                return processRegExpData(MASS_REGEXP, data);
            })
            .on('convertLength', function (data) {
                if (!data.message) return;
    
                var units = 'km|m|dm|cm|mm';
                var LENGTH_REGEXP = defineRegExpByUnits(units);
    
                return processRegExpData(LENGTH_REGEXP, data);
            })
            .on('convertUSDtoEUR', function (data) {
                if (!data.message) return;
    
                var rate = 0.89;
                var USD_TO_EUR_REGEXP = defineCurrencyRegExpByUnits('usd|USD', 'eur|EUR');
    
                convertCurrencyByRegExpAndRate(USD_TO_EUR_REGEXP, rate, data);
    
            })
            .on('convertEURoUSD', function (data) {
                if (!data.message) return;
    
                var rate = 1.12;
                var EUR_TO_USD_REGEXP = defineCurrencyRegExpByUnits('eur|EUR', 'usd|USD');
    
                convertCurrencyByRegExpAndRate(EUR_TO_USD_REGEXP, rate, data);
            })
            .on('send', function (data) {
                if (!data.message) return;
    
                io.sockets.emit('message', data);
            })
            .on('disconnect', function() {
                socket.broadcast.emit('leave', username);
                socket.emit('list',  getUsersList());
            });

         // Helper functions to DRY

        function getUsersList() {
            var users = [];
			var username;
			
            Object.keys(io.sockets.sockets).forEach(function(key) {
				username = (io.sockets.sockets[key].request.user.displayName) ? io.sockets.sockets[key].request.user.displayName : io.sockets.sockets[key].request.user.local.username;
                users.push(username);
            });

            return users;
        }

        function defineRegExpByUnits (units) {
            return new RegExp('^(\\d+)\\s(' + units + ')\\sto\\s(' + units + ')$', 'im');
        }

        function defineCurrencyRegExpByUnits (unitsFrom, unitsTo) {
            return new RegExp('^(\\d+)\\s(' + unitsFrom + ')\\sto\\s(' + unitsTo + ')$', 'im');
        }

        function convertCurrencyByRegExpAndRate(regExp, rate, data) {
            if (regExp.test(data.message)) {
                var matchData = data.message.match(regExp);
                var value = parseInt(matchData[1]);
                var currencySign = matchData[3];
                var result = (value * rate.toFixed(2));
                var message = '<span id=at>@' + data.username + '</span> result: ' + result + ' ' + currencySign;

                io.sockets.emit('message', {message: message});
            }
        }

        function processRegExpData(regExp, data) {
            if (regExp.test(data.message)) {
                try {
                    var evalResult = math.eval(data.message);
                    var message = '<span id=at>@' + data.username + '</span> result: ' + evalResult;
                    io.sockets.emit('message', {message: message});

                } catch (err) {
                    console.error(err);
                }
            }
        }
    });

    return io;
};
